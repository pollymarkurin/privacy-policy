**Privacy Policy**

Geniora (PT Multitech Infomedia) built the Geniora app as a Commercial app. This service provided by Geniora (PT Multitech Infomedia) and is intended for use as is.

This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our service.

If you choose to use our service, then you agree to the collection and use of information in relation to this policy. The personal information that we collect is used for providing and improving the service. We will not use or share your information with anyone except as described in this Privacy Policy. The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Geniora unless otherwise defined in this Privacy Policy.

**Information Collection and Use**

For a better experience, while using our service, we may require you to provide us with certain personally identifiable information, including but not limited to:

- Name
- Email
- Address
- School
- Education level
- Date of Birth
- Phone / contact number
- Demographic Location
- Photo uploaded for user profile picture

The information that we request will be retained by us and used as described in this privacy policy.

**Log Data**

We want to inform you that whenever you use our service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol (&quot;IP&quot;) address, device name, operating system version, the configuration of the app when utilizing our service, the time and date of your use of the service, and other statistics.

**Cookies**

Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device&#39;s internal memory.

This service does not use these &quot;cookies&quot; explicitly. However, the app may use third party code and libraries that use &quot;cookies&quot; to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this service.

**Camera**

Geniora456 accesses the phone&#39;s camera used for the purpose of scanning QR code in order to connect and/or casting images and videos to connected devices.

**Data Usage**

To operate and improve the App:

- Enable you to use the App&#39;s features;
- Establish and maintain your account, if you choose to login to the App using your social media account;
- Communicate with you about the App, including by sending you announcements, updates, and security alerts, which we may send through a push notification, and responding to your requests, questions and feedback;
- Provide technical support and maintenance for the App; and
- Perform statistical analysis about use of the App (including through the use of [Google Analytics](https://policies.google.com/technologies/partner-sites)).

To send you marketing and promotional communications. We may send you marketing communications as permitted by law. You will have the ability to opt-out of our marketing and promotional communications as described in the [Opt out of marketing](https://www.faceapp.com/privacy-en.html#opt-out-of-marketing-communications) section below.

For compliance, fraud prevention, and safety. We may use your personal information and disclose it to law enforcement, government authorities, and private parties as we believe necessary or appropriate to: (a) protect our, your or others&#39; rights, privacy, safety or property (including by making and defending legal claims); (b) enforce the terms and conditions that govern the service; and (c) protect, investigate and deter against fraudulent, harmful, unauthorized, unethical or illegal activity.

With your consent. In some cases, we may specifically ask for your consent to collect, use or share your personal information, such as when required by law.

To create anonymous, aggregated or de-identified data. We may create anonymous, aggregated or de-identified data from your personal information and other individuals whose personal information we collect. We make personal information into anonymous, aggregated or de-identified data by removing information that makes the data personally identifiable to you. We may use this anonymous, aggregated or de-identified data and share it with third parties for our lawful business purposes.

**Service Providers**

we may employ third-party companies and individuals due to the following reasons:

- To facilitate our service;
- To provide the service on our behalf;
- To perform service-related services; or
- To assist us in analyzing how our service is used.

we want to inform users of this service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.

**Security**

We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.

**Children&#39;s Privacy**

This piracy notice is intended to help parents understand what data is collected by the app, why we collect it, what we do with it, and our privacy controls. This Notice provides information about our privacy practices that are specific to Geniora456. This information includes the videos your child watches, their search terms, their answers and grades inside the quiz and exam from the app and other interactions with content and videos in the app.

Geniora 456 does not allow your child to share personal information with third parties or make it publicly available.

**Signing in to Geniora456 with your Google Account:**

If you sign in to Geniora456 with your Google Account, the app will collect your Google Account information, such as email address and password, and, as applicable, will collect profile information for any profiles you create for your child as well as your parental controls and customization preferences. The profile information will include your child&#39;s name or nickname, their age, and their month of birth if you choose to provide it. The app will collect customization preferences such as videos blocked from the app, videos you may allow in the app, and channels to which they may subscribe. The app also collects the child&#39;s app activity data including watch and search history specific to each child, as described above.

**Data retention**

Where you are a registered user of the site, we retain your data for as long as you remain an active user of your account. Where there has not been any activity on your account for three years we will contact you to check you still wish to retain your account with us. When you decide to cancel your account or we do not hear further from you, your account will be de-activated and your data will be deleted. Where you have subscribed to receive marketing correspondence from us we will keep your personal data for marketing purposes whilst your account remains active and for the period of time referred to above. This is subject to exercising your right to unsubscribe from receiving such correspondence at any time.

**Changes to This Privacy Policy**

We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.

This policy is effective as of 2020-04-27

**Contact Us**

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at:

Geniora

Rukan New Jasmine

Jl. Kelapa Gading Selatan Blok HA 1 No 50-51

Gading Serpong, Tangerang 15811 – Indonesia

Phone: +62 (21) 2222 8721

Email: info@geniora.com
Rendered
Privacy Policy

Geniora (PT Multitech Infomedia) built the Geniora app as a Commercial app. This service provided by Geniora (PT Multitech Infomedia) and is intended for use as is.

This page is used to inform visitors regarding our policies with the collection, use, and disclosure of Personal Information if anyone decided to use our service.

If you choose to use our service, then you agree to the collection and use of information in relation to this policy. The personal information that we collect is used for providing and improving the service. We will not use or share your information with anyone except as described in this Privacy Policy. The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Geniora unless otherwise defined in this Privacy Policy.

Information Collection and Use

For a better experience, while using our service, we may require you to provide us with certain personally identifiable information, including but not limited to:

Name
Email
Address
School
Education level
Date of Birth
Phone / contact number
Demographic Location
Photo uploaded for user profile picture
The information that we request will be retained by us and used as described in this privacy policy.

Log Data

We want to inform you that whenever you use our service, in a case of an error in the app we collect data and information (through third party products) on your phone called Log Data. This Log Data may include information such as your device Internet Protocol ("IP") address, device name, operating system version, the configuration of the app when utilizing our service, the time and date of your use of the service, and other statistics.

Cookies

Cookies are files with a small amount of data that are commonly used as anonymous unique identifiers. These are sent to your browser from the websites that you visit and are stored on your device's internal memory.

This service does not use these "cookies" explicitly. However, the app may use third party code and libraries that use "cookies" to collect information and improve their services. You have the option to either accept or refuse these cookies and know when a cookie is being sent to your device. If you choose to refuse our cookies, you may not be able to use some portions of this service.

Camera

Geniora456 accesses the phone's camera used for the purpose of scanning QR code in order to connect and/or casting images and videos to connected devices.

Data Usage

To operate and improve the App:

Enable you to use the App's features;
Establish and maintain your account, if you choose to login to the App using your social media account;
Communicate with you about the App, including by sending you announcements, updates, and security alerts, which we may send through a push notification, and responding to your requests, questions and feedback;
Provide technical support and maintenance for the App; and
Perform statistical analysis about use of the App (including through the use of Google Analytics).
To send you marketing and promotional communications. We may send you marketing communications as permitted by law. You will have the ability to opt-out of our marketing and promotional communications as described in the Opt out of marketing section below.

For compliance, fraud prevention, and safety. We may use your personal information and disclose it to law enforcement, government authorities, and private parties as we believe necessary or appropriate to: (a) protect our, your or others' rights, privacy, safety or property (including by making and defending legal claims); (b) enforce the terms and conditions that govern the service; and (c) protect, investigate and deter against fraudulent, harmful, unauthorized, unethical or illegal activity.

With your consent. In some cases, we may specifically ask for your consent to collect, use or share your personal information, such as when required by law.

To create anonymous, aggregated or de-identified data. We may create anonymous, aggregated or de-identified data from your personal information and other individuals whose personal information we collect. We make personal information into anonymous, aggregated or de-identified data by removing information that makes the data personally identifiable to you. We may use this anonymous, aggregated or de-identified data and share it with third parties for our lawful business purposes.

Service Providers

we may employ third-party companies and individuals due to the following reasons:

To facilitate our service;
To provide the service on our behalf;
To perform service-related services; or
To assist us in analyzing how our service is used.
we want to inform users of this service that these third parties have access to your Personal Information. The reason is to perform the tasks assigned to them on our behalf. However, they are obligated not to disclose or use the information for any other purpose.

Security

We value your trust in providing us your Personal Information, thus we are striving to use commercially acceptable means of protecting it. But remember that no method of transmission over the internet, or method of electronic storage is 100% secure and reliable, and we cannot guarantee its absolute security.

Children's Privacy

This piracy notice is intended to help parents understand what data is collected by the app, why we collect it, what we do with it, and our privacy controls. This Notice provides information about our privacy practices that are specific to Geniora456. This information includes the videos your child watches, their search terms, their answers and grades inside the quiz and exam from the app and other interactions with content and videos in the app.

Geniora 456 does not allow your child to share personal information with third parties or make it publicly available.

Signing in to Geniora456 with your Google Account:

If you sign in to Geniora456 with your Google Account, the app will collect your Google Account information, such as email address and password, and, as applicable, will collect profile information for any profiles you create for your child as well as your parental controls and customization preferences. The profile information will include your child's name or nickname, their age, and their month of birth if you choose to provide it. The app will collect customization preferences such as videos blocked from the app, videos you may allow in the app, and channels to which they may subscribe. The app also collects the child's app activity data including watch and search history specific to each child, as described above.

Data retention

Where you are a registered user of the site, we retain your data for as long as you remain an active user of your account. Where there has not been any activity on your account for three years we will contact you to check you still wish to retain your account with us. When you decide to cancel your account or we do not hear further from you, your account will be de-activated and your data will be deleted. Where you have subscribed to receive marketing correspondence from us we will keep your personal data for marketing purposes whilst your account remains active and for the period of time referred to above. This is subject to exercising your right to unsubscribe from receiving such correspondence at any time.

Changes to This Privacy Policy

We may update our Privacy Policy from time to time. Thus, you are advised to review this page periodically for any changes. We will notify you of any changes by posting the new Privacy Policy on this page.

This policy is effective as of 2020-04-27

Contact Us

If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at:

Geniora

Rukan New Jasmine

Jl. Kelapa Gading Selatan Blok HA 1 No 50-51

Gading Serpong, Tangerang 15811 – Indonesia

Phone: +62 (21) 2222 8721

Email: info@geniora.com

Want to convert another document?

Feedback
Source
Donate
Terms
Privacy
@benbalter